#Türkiye’de Dijital Gözetim: T.C. Kimlik Numarasından E-kimlik Kartlarına Yurttaşın Sayısal Bedenlenişi  

 Alternatif Bilişim Derneği yeni bir kitapta bu kez dijital gözetimi tartışmaya açıyor. Ankara Üniversitesi Sosyal Bilimler Enstitüsü'nde 2011 Bahar Dönemi'nde Yeni Medya Sosyolojisi adlı doktora dersinde bir araya gelen bir grup akademisyen, Türkiye'de dijital gözetim konusuna çalıştı. Daha sonra o çalışma, süre gelen tartışmalar ve araştırmalarla beslendi ve kolektif bir emeğin ürünü olarak Türkiye’de Dijital Gözetim: T.C. Kimlik Numarasından E-kimlik Kartlarına Yurttaşın Sayısal Bedenlenişi kitabı oldu.

Kitap vatandaşını an be an izleyen ‘devlet’ ile gözetlenirken bir başkasını gözetleyen vatandaşı ve yarattığı dönüşümü ele alıyor. Modern devletlerin yeni iletişim teknolojileri yoluyla toplumsal yaşamı ve yurttaşları artan oranda nasıl denetlediğini ve kontrol ettiğini ortaya koyuyor. Gözetimin tüm toplumun yaşamına sirayet ettiği günümüzde yurttaşların nasıl ‘kişiliklerinden soyutlanıp’ birer sayısal bedene dönüştüğünü gözler önüne seriyor.

Kitabın “Dijital Gözetim Olgusuna Kuramsal ve Kavramsal Bakış” başlıkla birinci bölümü, dijital gözetim olgusunun ele alınabilmesi için kavramsal bir çerçeve sunuyor. Günümüzün yeniden yapılandırmacı muhafazakarlık biçimi olan neoliberal söylemin şeffaflık, yönetişim, demokrasi gibi kavramlarının ideolojik olarak nasıl kavranabileceğini detay ve örneklerle açıklıyor.

“Dijital Gözetim Teknolojileri ve Uygulamaları” başlıklı ikinci bölüm ise dijital gözetim amacıyla kullanılan teknolojileri ele alıyor. Bunların başında İnternet iletişiminin takip altına alınması anlamına gelen DPI (Deep packet inspection - Derin paket sorgulama) geliyor. ABD'nin İnternet iletişimini kayıtlaması, Türkiye'de BTK'nın "Güvenli İnternet" filtre sistemi, devletlerin güvenlik kameraları ile tüm alanlarda kurdukları denetim, Türkiye'de son yıllarda oluşan yargı rejiminin "delil" ve "suçlu" bulma çalışmaları, genetik bilgilerimizin bile bir denetim nesnesi olabileceği gerçeği olarak biyopolitika, işyerleri ve çalışma alanlarında yetkililerin hiç konuşmadan, gizlice sürdürdükleri takipleme ve ölçme-tartma çalışmaları, Google gibi araçların bizi izleyip kişiliğimizi ölçerek yanıt döndürmesi, bu şirketlerde kayıtlanan kişisel bilgilerin kötüye kullanılması tehlikesi...

Üçüncü bölüm, tekrar Türkiye'ye dönüyor ve Osmanlı'dan günümüze "nüfus idaresi" ile başlayıp "MERNİS" projesi olarak devam eden yurttaşların kayıtlanması sürecini anlatıyor. “Türkiye’de Yurttaşların Sayısal Kayıtlanması ve Gözetlenmesi: Şecere Kayıtlarından Sayısal Bedenlenişe” başlıklı bu bölümde ise 19. yüzyıldan MERNİS’e ve biyometrik kimlik kartlarına geçiş inceleniyor. Türkiye Cumhuriyeti vatandaşlarının tüm bilgilerinin tek bir merkezi anahtar: T.C. kimlik numarasıyla erkezileştirilmesi, bireyin doğumdan ölüme, gündelik yaşamın her alanında devlet tarafından dijital olarak kayıtlanıp, sınıflara ayrılmasına dikkat çekiliyor. Özgürlüklerin otoritenin kontrolünün yanı sıra sürekli gözetlendiğini ve kontrol edildiğini düşünen yurttaşlar tarafından da kısıtlandığına vurgu yapılan çalışmada bu derece bireye özel olan verilerin ise hala korunaksız olduğuna dikkat çekiliyor. Bu sorun özellikle kişisel verilerin korunması ve güvenliği konusunda dünyadaki ve Türkiye’deki durumun ayrıntılı bir şekilde ele alındığı dördüncü bölümde ele alınıyor. Kitapta son olarak, Türkiye’de yurttaşın TC. Kimlik numarası ile olsun e-kimlik kartları uygulaması ile olsun sayısal bedenlenişi konusunda genel değerlendirme yapılarak, bu konuda yurttaşı ve yurttaşın kişisel verilerinin güvenliği ile kişisel verileri üzerinde enformasyon hakkını merkeze alan öneriler geliştiriliyor.

Selma Arslantaş Toktaş, Mutlu Binark, Şafak Dikmen, Işık Barış Fidaner, Gülden Gürsoy-Ataman, Elif Küzeci, Alkım Özaygen’in yazdığı kitabı Mutlu Binark yayına hazırladı.

Türkiye’de Dijital Gözetim: T.C. Kimlik Numarasından E-kimlik Kartlarına Yurttaşın Sayısal Bedenlenişi kitabı Friedrich-Ebert-Stiftung Vakfı Türkiye Temsilciliği-İstanbul’un katkılarıyla basıldı. Kitap Alternatif Bilişim Derneği’nin daha önce yayınladığı iki kitap gibi özgür e-kitap olarak da derneğin sitesinden indirilebilecek.

Basılı kitap talebi için: bilgi@alternatifbilisim.org

**Türkiye’de Dijital Gözetim:**  
*T.C. Kimlik Numarasından E-kimlik Kartlarına Yurttaşın Sayısal Bedenlenişi*  
**ISBN**: 978-605-62169-2-3  
Mayıs 2012, 184 Sayfa  
**Yayına hazırlayan**: Mutlu Binark  
**Kapak**: Şafak Dikmen  
**Latex Yerleşimi**: Alkım Özaygen  
**Baskı**: Ceylan Mat  
Yazıların hakları yazarlara aittir.  
Kitabın LaTeX kodları CC Attribution-NonCommercial 3.0 Unported License altındadır.  

##İçindekiler

###Sunu / Sunuş / Önsöz

###Bölüm I: DİJİTAL GÖZETİM OLGUSUNA KURAMSAL ve KAVRAMSAL BAKIŞ

    Neoliberal Toplumun İktidar Modeli: Yönetişim ve E-devlet
    Güvenlik Adına Risk Yönetiminin Uygulanması
    Devletin Yurttaşını Gözetlemesi Olgusunun Nedenleri
    Modern Dönemin Panoptik Stratejileri
    Kusursuz Bir İktidar İçin Sürekli Denetim: Biyo-Politika
    Gözetim ve Denetimin Her Yerdeliğinin Normalleşmesi ve İçselleştirilmesi
    Panoptikondan Süper Panoptikona
    Sonuç olarak

###Bölüm II: DİJİTAL GÖZETİM TEKNOLOJİLERİ VE UYGULAMALARI

    Askeri ve İstihbarat Alanları
    Devlet Yönetimi, Nüfus Sayımı ve Suç Kontrolü Alanları
    İş Yeri Gözetimi ve Denetimi Alanları
    Tüketim ve Tüketici Yapılandırması Alanı

###Bölüm III: TÜRKİYE’DE YURTTAŞLARIN SAYISAL KAYITLANMASI VE GÖZETLENMESİ: ŞECERE KAYITLARINDAN SAYISAL BEDENLENİŞE

    Türkiye’de Yurttaşın Sayısal Kayıtlanmasının Kısa Tarihçesi
    Araştırmanın Yöntemi: E-devlet Kapısı Üzerinde Yurttaşın Sayısal Bedenlenişinin Topografyasını Çıkartmak
    Türkiye’de Yurttaşın Sayısal Bedenlenişinin Topografyası

###Bölüm IV: TC KİMLİK NUMARASI: HUKUKSAL BİR DEĞERLENDİRME

    Giriş
    Modern Devletin "Veri Açlığı" ve Hızla Şeffaflaşan Bireyin Hukuksal Kalkanı Elektronik Devlet, Sayısallaştırılmış Yurttaş
    “Ben” ya da On Bir Hanelik Kimlik Numaram
    Dijital Çağda Bireysel Özerklik Nasıl Sürdürülebilir?

###Bölüm V: GENEL DEĞERLENDİRME

###Ekler 1-2-3-4

###Kaynakça

###Özet

###Abstract

###Özgeçmişler
